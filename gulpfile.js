const gulp = require("gulp");
const uglify = require("gulp-uglify");
const liveReload = require("gulp-livereload");
const autoprefixer = require("gulp-autoprefixer");
const plumber = require("gulp-plumber");
const sourcemaps = require("gulp-sourcemaps");
const minifyCss = require("gulp-minify-css");

//FOR CSS ONLY
const concat = require("gulp-concat");

//FOR SASS ONLY (does not require minifyCSS)
const sass = require("gulp-sass");

//FOR LESS ONLY
const less = require("gulp-less");
const LessAutoPrefix = require("less-plugin-autoprefix");
const lessAutoPrefix = new LessAutoPrefix({
  browsers: ["last 2 version"]
});

// local vars
const DIST_PATH = "./public/dist";
const CSS_PATH = "./public/css/**/*.css";
const SCRIPTS_PATH = "./public/scripts/**/*.js";
const SCSS_PATH = "./public/scss/";
const LESS_PATH = "./public/less/";

const srcImages = "";
const distImages = "";

// //Styles
// gulp.task("styles", function() {
//   console.log("Starting styles task");
//   return gulp
//     .src(["public/css/reset.css", CSS_PATH])
//     .pipe(
//       plumber(function(e) {
//         console.log("Styles Task Error");
//         console.log(e);
//         this.emit("end");
//       })
//     )
//     .pipe(sourcemaps.init())
//     .pipe(
//       autoprefixer({
//         browsers: ["last 2 versions", "ie 8"]
//       })
//     )
//     .pipe(concat("styles.css"))
//     .pipe(minifyCss())
//     .pipe(sourcemaps.write())
//     .pipe(gulp.dest(DIST_PATH))
//     .pipe(liveReload());
// });

// Styles for SASS

// gulp.task("scss", function() {
//   console.log("Starting scss task");
//   return gulp
//     .src([SCSS_PATH + "styles.scss"])
//     .pipe(
//       plumber(function(e) {
//         console.log("scss Task Error");
//         console.log(e);
//         this.emit("end");
//       })
//     )
//     .pipe(sourcemaps.init())
//     .pipe(autoprefixer())
//     .pipe(
//       sass({
//         outputStyle: "compressed"
//       })
//     )
//     .pipe(sourcemaps.write())
//     .pipe(gulp.dest(DIST_PATH))
//     .pipe(liveReload());
// });

//Styles for LESS

gulp.task("less", function() {
  console.log("Starting less task");
  return gulp
    .src([LESS_PATH + "styles.less"])
    .pipe(
      plumber(function(e) {
        console.log("LESS Task Error");
        console.log(e);
        this.emit("end");
      })
    )
    .pipe(sourcemaps.init())
    .pipe(
      less({
        plugins: [lessAutoPrefix]
      })
    )
    .pipe(minifyCss())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(DIST_PATH))
    .pipe(liveReload());
});

//Scripts
gulp.task("scripts", function() {
  console.log("Starting scripts task");

  return gulp
    .src(SCRIPTS_PATH)
    .pipe(
      plumber(function(e) {
        console.log("Scripts Task Error");
        console.log(e);
        this.emit("end");
      })
    )
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(gulp.dest(DIST_PATH))
    .pipe(sourcemaps.write())
    .pipe(liveReload());
});

//Images
gulp.task("images", function() {
  console.log("Starting images task");
});

//Default
gulp.task("default", function() {
  console.log("Starting default task");
});

gulp.task("watch", function() {
  console.log("Starting gulp watch");
  require("./server.js");
  liveReload.listen();
  gulp.watch(SCRIPTS_PATH, ["scripts"]);
  //gulp.watch(CSS_PATH, ["styles"]);
  //gulp.watch(SCSS_PATH + "**/*.scss", ["scss"]);
  gulp.watch(LESS_PATH + "**/*.less", ["less"]);
});
