# GULP-template
Basic gulpfile.js template (includes SASS, CSS, LESS, JS, IMG)

You can pull down/fork the entire repo to act as a skeleton for your project or you can just use the package.json devDependencies,
and gulpfile.js as a starting point.

I highly recommend visting Andrew Meads GULP course on UDEMY.  He provides excellent guidance and practical work-a-longs. 
This template project was built using this course.  https://www.udemy.com/learn-gulp

## Requirements
### System apps 
Node.JS

### Global Node Modules
gulp
static-server (or dev server of your choice)

## Instructions.
1.  Make sure Node.js is installed, and gulp is installed globally using npm

2.  If you are pulling down/forking the entire project skeleton, you will need to run npm install from the project folder root. 

3.  If you are just using the package.json devDependencies and gulpfile.js copy as necessary and run npm install in the same folder.

4.  Once the dependencies are installed, you should delete any folders you don't need as well as the public/dist folder.  

5.  Finally comment/uncomment/delete code blocks in gulpfile.js as necessary to suit your project.

## View the code and fork it

<https://github.com/StuLast/GULP-template>

